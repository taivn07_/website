<?php
/*
Plugin Name: Contact Form Captcha
Description: Extend the Contact Form to include a Captcha to protect against spam bots.
Version: 1.0
Author: Jeff Bullins
Author URI: http://www.thinklandingpages.com
*/


//adapted from https://github.com/easydigitaldownloads/EDD-Extension-Boilerplate/blob/master/plugin-name/plugin-name.php
//include_once 'custom-post-type.php';  

if( !defined( 'ABSPATH' ) ) exit;
if( !class_exists( 'Contact_Form_Captcha' ) ) {
    /**
     * Main Contact_Form_Captcha class
     *
     * @since       1.0.0
     */
    class Contact_Form_Captcha {
    
    	private static $instance;
        /**
         * Get active instance
         *
         * @access      public
         * @since       1.0.0
         * @return      object self::$instance The one true Contact_Form_Captcha
         */
        public static function instance() {
            if( !self::$instance ) {
                self::$instance = new Contact_Form_Captcha();
                self::$instance->setup_constants();
                //self::$instance->includes();
                //self::$instance->load_textdomain();
                self::$instance->hooks();
            }
            return self::$instance;
        }
        
        private function hooks(){
        	add_filter('cf_add_to_form_filter', array( $this, 'add_simple_addition_captcha'));    
        	add_action('wp_enqueue_scripts', array($this, 'enqueue_header_scripts'  ));  	
        	add_action('admin_enqueue_scripts', array($this, 'enqueue_header_scripts'  ));  
        	
        }
        
        public function add_simple_addition_captcha(){
        	//ob_start();
        	$first = mt_rand(0, 9);
        	$second = mt_rand(0, 9);
        	$answer = $first + $second;
        	echo '<input type="hidden" name="sol" value="'.$answer.'">';
        	echo $first . ' +  '.$second.' = <input type="text" name="answer" size="4"><br>';
        	//return ob_get_clean();
        }
        
        
        
                /**
         * Setup plugin constants
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         */
        private function setup_constants() {
            // Plugin version
            define( 'CONTACT_FORM_CAPTCHA_VER', '1.0.0' );
            // Plugin path
            define( 'CONTACT_FORM_CAPTCHA_DIR', plugin_dir_path( __FILE__ ) );
            // Plugin URL
            define( 'CONTACT_FORM_CAPTCHA', plugin_dir_url( __FILE__ ) );
        }
        
        
         /**
         * Include necessary files
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         */
        private function includes() {
        	
            // Include scripts
            //require_once PGU_FOUNDATION_DIR . 'includes/scripts.php';
            //require_once PGU_FOUNDATION_DIR . 'includes/functions.php';
            /**
             * @todo        The following files are not included in the boilerplate, but
             *              the referenced locations are listed for the purpose of ensuring
             *              path standardization in PGU extensions. Uncomment any that are
             *              relevant to your extension, and remove the rest.
             */
            // require_once PGU_FOUNDATION_DIR . 'includes/shortcodes.php';
            // require_once PGU_FOUNDATION_DIR . 'includes/widgets.php';
        }
        
        
  /*      
        function enqueue_footer_scripts(){
		wp_enqueue_script('post-gallery-foundation-js', plugin_dir_url(__FILE__)."foundation/js/foundation/foundation.js");
		wp_enqueue_script('post-gallery-foundation-custom-js', plugin_dir_url(__FILE__).'js/foundationFooter.js');
	}
  */ 
	function enqueue_header_scripts(){
		wp_enqueue_script("contact-form-captcha", plugin_dir_url(__FILE__)."js/contact-form-captcha.js");
	}

 
    } //end class Contact_Form_Captcha
} //end Contact_Form_Captcha class exist check



function Contact_Form_Captcha_load() {
/*
    if( ! class_exists( 'PostGalleryCustomPostType' ) ) {
        if( ! class_exists( 'PGU_Extension_Activation' ) ) {
            require_once 'includes/class.extension-activation.php';
        }
        $activation = new PGU_Extension_Activation( plugin_dir_path( __FILE__ ), basename( __FILE__ ) );
        $activation = $activation->run();
        return Contact_Form_Captcha::instance();
    } else {
        return Contact_Form_Captcha::instance();
    }
*/
	return Contact_Form_Captcha::instance();
}

add_action( 'plugins_loaded', 'Contact_Form_Captcha_load' );


function pgu_foundation_activate() {
	//$postGalleryCustomPostType = new PostGalleryCustomPostType();
	//$postGalleryCustomPostType->create_post_type();
	//global $wp_rewrite;
	//$wp_rewrite->flush_rules();
}


register_activation_hook( __FILE__, 'pgu_foundation_activate');

 