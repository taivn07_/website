<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,user-scalable=yes">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src='<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js'></script>
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/libs/css/animations.css" type="text/css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/libs/example-assets/style.css" type="text/css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<script src='<?php echo get_template_directory_uri(); ?>/css/libs/js/css3-animate-it.js'></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css" type="text/css" />
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/site.css" type="text/css" />
	
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jssor.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jssor.slider.js"></script>
	<!-- <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/wow.js"></script> -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script> 
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.counterup.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
	
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	
	<![endif]-->
	<?php wp_head(); ?>
</head>
<script>
$(document).scroll(function() {
	var device_width = document.documentElement.clientWidth;
	if(device_width>1024)
	{
		var y = $(this).scrollTop();
		if (y > 50) {
			$('.menu_logo').css("top","0");
			$('.menu_logo').css("background-color","#7049ba");
			$('.menu_logo').css("position","fixed");
			$('.menu_logo').css("z-index","999999");
		}
		else
		{
			$('.menu_logo').css("top","30px");
			$('.menu_logo').css("background-color","");
			$('.menu_logo').css("position","fixed");
			$('.menu_logo').css("z-index","999999");
		}
	}
	if (y > 100) {
		$( ".w-toplink" ).addClass( "active" );
	}
	else
	{
		$( ".w-toplink" ).removeClass( "active" );
	}
});

$(document).ready(function()
{

	var height = $(window).height();
	var width = $(window).width();
	if(width > 640)
	{
		$("#navbar").css("max-width",width-270);
	}
	if(width >1024)
	{
		var navbar_width  = $(".main-navigation").width();
		list_a = $("#primary-menu").find("a");	
		var width_a = 0;
		for(var i =0;i<list_a.length;i++)
		{
			width_a += $(list_a[i]).width();
		}
		var padding_a = (navbar_width-width_a-20)/14;
		for(var i =0;i<list_a.length;i++)
		{
			$(list_a[i]).css("padding-top","10px");
			$(list_a[i]).css("padding-right",padding_a);
			$(list_a[i]).css("padding-bottom","10px");
			$(list_a[i]).css("padding-left",padding_a);	
		}
		
	}
	else
	{
		list_a = $(".menu-menu-container").find("a");	
		var width_a = 0;
		for(var i =0;i<list_a.length;i++)
		{
			width_a += $(list_a[i]).width();
		}
		
		var padding_a = (width-width_a-20)/14;
		for(var i =0;i<list_a.length;i++)
		{
			$(list_a[i]).css("padding-top","10px");
			$(list_a[i]).css("padding-right",padding_a);
			$(list_a[i]).css("padding-bottom","10px");
			$(list_a[i]).css("padding-left",padding_a);	
		}
	}
	$(".slideshow_slide_image img").css("height",height);
	$(".list_img_more").hover(function(){
        $('.ripple-container').css("display", "block");
        }, function(){
        $('.ripple-container').css("display", "none");
    });
	$('#primary-menu li a').click(function(){
		var href = $(this).attr('href');
		href = href.substring(1);
		var anchor = $('.'+href).offset();
		$('body').animate({ scrollTop: anchor.top - 100 });
		return false;
	});
	$(window).resize(function() {
		var width = $(window).width();
		if(width > 640)
		{
			$("#navbar").css("max-width",width-270);
		}
		
		if(width >1024)
		{
			var navbar_width  = $(".main-navigation").width();
			list_a = $("#primary-menu").find("a");	
			var width_a = 0;
			for(var i =0;i<list_a.length;i++)
			{
				width_a += $(list_a[i]).width();
			}
			var padding_a = (navbar_width-width_a-20)/14;
			for(var i =0;i<list_a.length;i++)
			{
				$(list_a[i]).css("padding-top","10px");
				$(list_a[i]).css("padding-right",padding_a);
				$(list_a[i]).css("padding-bottom","10px");
				$(list_a[i]).css("padding-left",padding_a);	
			}
			
		}
		else
		{
			list_a = $("#primary-menu").find("a");	
			var width_a = 0;
			for(var i =0;i<list_a.length;i++)
			{
				width_a += $(list_a[i]).width();
			}
			var padding_a = (width-width_a-20)/14;
			for(var i =0;i<list_a.length;i++)
			{
				$(list_a[i]).css("padding-top","10px");
				$(list_a[i]).css("padding-right",padding_a);
				$(list_a[i]).css("padding-bottom","10px");
				$(list_a[i]).css("padding-left",padding_a);	
			}
		}
		var device_width = document.documentElement.clientWidth;
		if(device_width>1024)
		{
			var y = $(this).scrollTop();
			if (y > 50) {
				$('.menu_logo').css("top","0");
				$('.menu_logo').css("background-color","#7049ba");
				$('.menu_logo').css("position","fixed");
				$('.menu_logo').css("z-index","999999");
			}
			else
			{
				$('.menu_logo').css("top","30px");
				$('.menu_logo').css("background-color","");
				$('.menu_logo').css("position","fixed");
				$('.menu_logo').css("z-index","999999");
			}
		}
	});

});

</script>
<body <?php body_class(); ?>>
	<a class="w-toplink" href="#"><i class="fa fa-angle-up"></i></a>
	<div id="page" class="hfeed site">
		<header id="masthead" role="banner">
			
			<?php echo do_shortcode('[rev_slider Slider1]'); ?>
			
			<div class="menu_logo">
				<div class="menu_top">
					<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" />
					</a>

					<div id="navbar" class="navbar">
						<nav id="site-navigation" class="navigation main-navigation" role="navigation">
							<button class="menu-toggle"><?php _e( '|||', 'twentythirteen' ); ?></button>
							<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
							<?php wp_nav_menu( array( 'menu' => 'menu', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
						
						</nav><!-- #site-navigation -->
							<div class="change_language">
								<?php if ( function_exists( 'the_msls' ) ) the_msls(); ?>
							</div>
					</div><!-- #navbar -->
					
				</div>
			</div>
		</header><!-- #masthead -->

		<div id="main" class="site-main">
