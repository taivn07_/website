<?php 

/* 
Template Name: Home
*/


get_header();

?>
<script src='<?php echo get_template_directory_uri(); ?>/css/libs/js/css3-animate-it.js'></script>
<script>
	// wow = new WOW(
	  // {
		// animateClass: 'animated',
		// offset:       100,
		// callback:     function(box) {
		  // console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
		// }
	  // }
	// );
	// wow.init();
   $(document).ready(function()
   {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });
</script>
<div class="content">
	<?php dynamic_sidebar( 'First-Position' ); ?>
	<div class="founder">
		<div class="founder_center animatedParent">
			<h2 class="bounceInUp animated"><strong>Co-Founder</strong></h2>
			<p style="text-align: center;">Limitless options beautifully packed in a brand-new premium WordPress theme.</p>
		
			<div class="founder_left animated bounceInLeft" data-wow-delay="0s">
				<img src="<?php echo get_template_directory_uri(); ?>/images/ipad-iphone.png" />
			</div>
			<div class="founder_right animated bounceInRight" data-wow-delay="0s">
				<div class="w-iconbox iconpos_left size_small style_default color_contrast">
					<div class="w-iconbox-icon"><i class="fa fa-camera-retro"></i></div>
					<h4 class="w-iconbox-title">Retina Ready &amp; Fully Responsive</h4>
					<div class="w-iconbox-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
				</div>
				<div class="w-separator type_invisible size_small thick_1 style_solid color_border cont_none">
					<span class="w-separator-h"></span>
				</div>
				<div class="w-iconbox iconpos_left size_small style_default color_contrast">
					<div class="w-iconbox-icon"><i class="fa fa-image"></i></div>
					<h4 class="w-iconbox-title">Four Awesome Header Layouts</h4>
					<div class="w-iconbox-text">Consectetur adipiscing elit, lorem ipsum dolor sit amet.</div>
				</div>
				<div class="w-separator type_invisible size_small thick_1 style_solid color_border cont_none">
					<span class="w-separator-h"></span>
				</div>
				<div class="w-iconbox iconpos_left size_small style_default color_contrast">
					<div class="w-iconbox-icon"><i class="fa fa-comments-o"></i></div>
					<h4 class="w-iconbox-title">Quick-to-Install and Easy-to-Use</h4>
					<div class="w-iconbox-text">Dolor lorem ipsum sit amet, adipiscing elit consectetur.</div>
				</div>
			</div>
		</div>
		
		
	</div>
	<div class="gallery_list ">
		<div class="l-section-img" style="background-image: url(http://zephyr.us-themes.com/wp-content/uploads/colorful-266993_1920.jpg)"></div>
		<div class="l-section-overlay" style="background-color: rgba(0,0,0,0.4)"></div>
		<div class="gallery_list_center animatedParent" data-sequence='500'>
			<h2 class="bounceInUp animated" data-id=1><strong>LATEST PROJECTS</strong></h2>
			<div class="list_img ">
				<ul>
					<?php
					for($i=0;$i<6;$i++)
					{
							$j = $i+2;
					?>
					<li class="bounceInLeft animated view view-third" data-id="<?php echo $j;?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/picjumbo.jpg" />
						<div class="mask mask<?php echo $i;?>">
							<div class="info_img">
								<h3>Single Project – Slider</h3>
								<p>Photography</p>
							</div>
							
						</div>
					</li>
					<?php
					}
					?>
					
					
				</ul>
			</div>
			<div class="w-separator type_invisible size_medium thick_1 style_solid color_border cont_none"><span class="w-separator-h"></span></div>
			<a class="list_img_more" href="#">
				<i class="fa fa-long-arrow-right"></i>
				<span>More Info</span>
				<span class="ripple-container">
					<span class="ripple ripple-on ripple-out" style="left: 667px; top: 247px; transform: scale(27.625);"></span>
				</span>
			</a>
			<div class="w-separator type_invisible size_medium thick_1 style_solid color_border cont_none"><span class="w-separator-h"></span></div>
		</div>
		
		
	</div>
	<div class="imgsize_cover animatedParent">
		<div class="imgsize_cover_left  animated  bounceInLeft" >
			<h2>Wonderful digital things require<br>a good mix of combined skills</h2>
			<p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in purus. Praesent viverra, est vitae efficitur auctor, nibh orci varius ligula, gravida molestie enim justo ut diam. Praesent eget congue lacus, ut viverra neque.</p>
			<a href="#" class="Features"><i class="fa fa-star-o"></i> See Features</a>
			<a href="#" class="Learn_more">Learn More <i class="fa fa-angle-right"></i></a>
			
		</div>
		<div class="imgsize_cover_right animated  bounceInRight">
			<img src="<?php echo get_template_directory_uri(); ?>/images/iPhone-6-Infinity1.png" />
		</div>
	</div>
	<div class="product_count animatedParent">
		<div class="l-section-img1" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/picjumbo.com_HNCK2634.jpg)"></div>
		<div class="l-section-overlay" style="background-color: rgba(17,17,17,0.5)"></div>
		<div class="product_count_info">
			<div class="wpb_wrapper">
				<h2><strong>ZEPHYR. </strong>NEW EDGE OF THE THEME EXPERIENCE!</h2>
			</div>
			<div class="intro wow flipInX center animated">
				<span class="counter" style="display: inline-block; width: 32%">72</span>
				<h6>Button combinations</h6>
			</div>
			<div class="intro wow flipInX center animated">
				<span class="counter" style="display: inline-block; width: 32%">72</span>
				<h6>Button combinations</h6>
			</div>
			<div class="intro wow flipInX center animated">
				<span class="counter" style="display: inline-block; width: 32%">72</span>
				<h6>Button combinations</h6>
			</div>
			<div class="intro wow flipInX center animated">
				<span class="counter" style="display: inline-block; width: 32%">72</span>
				<h6>Button combinations</h6>
			</div>
		</div>
	</div>
	<div class="answer_info animatedParent" data-sequence='500'>
		<div class="answer_info_left one_third animated fadeInUp" data-id=1>
			<div class="info_box">
				<div class="info_box_icon">
					<i class="fa fa-star-o"></i>
				</div>
				<h4>Modern Design & Trendy Look</h4>
				<p>Lorem ipsum dolor sit amet. Proin non blandit nibh. Sed eget tortor tincidunt, auctor sem eget.</p>
			</div>
			<div class="info_box">
				<div class="info_box_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<h4>Focus on Usability & User-experience</h4>
				<p>Lorem ipsum dolor sit amet, consec adipiscing elit. Proin non blandit nibh. Sed eget tortor tincidunt.</p>
				
			</div>
		</div>
		<div class="answer_info_center one_third animated fadeInUp" data-id=2>
			<div class="info_box">
				<div class="info_box_icon">
					<i class="fa fa-star-o"></i>
				</div>
				<h4>Modern Design & Trendy Look</h4>
				<p>Lorem ipsum dolor sit amet. Proin non blandit nibh. Sed eget tortor tincidunt, auctor sem eget.</p>
			</div>
			<div class="info_box">
				<div class="info_box_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<h4>Focus on Usability & User-experience</h4>
				<p>Lorem ipsum dolor sit amet, consec adipiscing elit. Proin non blandit nibh. Sed eget tortor tincidunt.</p>
				
			</div>
		</div>
		<div class="answer_info_right one_third animated fadeInUp" data-id=3>
			<div class="info_box">
				<div class="info_box_icon">
					<i class="fa fa-star-o"></i>
				</div>
				<h4>Modern Design & Trendy Look</h4>
				<p>Lorem ipsum dolor sit amet. Proin non blandit nibh. Sed eget tortor tincidunt, auctor sem eget.</p>
			</div>
			<div class="info_box">
				<div class="info_box_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<h4>Focus on Usability & User-experience</h4>
				<p>Lorem ipsum dolor sit amet, consec adipiscing elit. Proin non blandit nibh. Sed eget tortor tincidunt.</p>
			</div>
		</div>
	</div>
</div>


<?php
get_footer();

?>

