<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<script>
$(document).ready(function()
{
	var device_width = document.documentElement.clientWidth;
	if(device_width <= 640)
	{
		$('.footer_right iframe').attr("width",device_width);
	}
	$(window).resize(function()
	{
		var device_width = document.documentElement.clientWidth;
		$('.footer_right iframe').attr("width",device_width);
	});
});
</script>
		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="footer_center">
				<div class="footer_left one_third">
					<h4>Some Text</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla suctus.</p>
				</div>
				<div class="footer_center1 one_third">
					<h4>Liên Hệ</h4>
					<?php echo do_shortcode("[think_contact]")?>
				</div>
				<div class="footer_right one_third">
					<h4>Bản Đồ</h4>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7449.809974901869!2d105.82735040000003!3d20.99644540000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac88418718df%3A0xaab34eb0bbc1ea0c!2zMzY1IE5ndXnhu4VuIE5n4buNYyBO4bqhaSwgS2jGsMahbmcgVHJ1bmcsIFRoYW5oIFh1w6JuLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2sus!4v1443693137147" width="600" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- #page -->
	
	<?php wp_footer(); ?>
</body>
</html>